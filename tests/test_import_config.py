import os
import unittest

import import_config


class TestJson(unittest.TestCase):
    def setUp(self):
        self.TEST_CONFIG_FILE_ABS = "/tmp/tmpConfig_abs.json"
        self.TEST_CONFIG_FILE_REL = "./tests/tmpConfig_rel.json"

    def test_abs(self):
        """Test custom JSON file import with absolute path"""
        with open(self.TEST_CONFIG_FILE_ABS, "w") as f:
            f.write('{"test": "config"}')
        config = import_config.get_json(self.TEST_CONFIG_FILE_ABS)
        os.remove(self.TEST_CONFIG_FILE_ABS)
        self.assertEqual(config["test"], "config")

    def test_rel(self):
        """Test custom JSON file import with relative path"""
        with open(self.TEST_CONFIG_FILE_REL, "w") as f:
            f.write('{"test": "config"}')
        config = import_config.get_json(self.TEST_CONFIG_FILE_REL)
        os.remove(self.TEST_CONFIG_FILE_REL)
        self.assertEqual(config["test"], "config")


class TestYaml(unittest.TestCase):
    def setUp(self):
        self.TEST_CONFIG_FILE_ABS = "/tmp/tmpConfig_abs.yml"
        self.TEST_CONFIG_FILE_REL = "./tests/tmpConfig_rel.yml"

    def test_abs(self):
        """Test custom YAML file import with absolute path"""
        with open(self.TEST_CONFIG_FILE_ABS, "w") as f:
            f.write("test: config")
        config = import_config.get_yaml(self.TEST_CONFIG_FILE_ABS)
        os.remove(self.TEST_CONFIG_FILE_ABS)
        self.assertEqual(config["test"], "config")

    def test_rel(self):
        """Test custom YAML file import with relative path"""
        with open(self.TEST_CONFIG_FILE_REL, "w") as f:
            f.write("test: config")
        config = import_config.get_yaml(self.TEST_CONFIG_FILE_REL)
        os.remove(self.TEST_CONFIG_FILE_REL)
        self.assertEqual(config["test"], "config")


if __name__ == "__main__":
    unittest.main()
