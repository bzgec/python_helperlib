import os
import shutil
import sys
import unittest
from io import StringIO
from unittest.mock import patch

import helper


class TestStr(unittest.TestCase):
    def test_bold(self):
        """Test bold_str"""
        start = "\033[1m"
        end = "\033[0m"
        self.assertEqual(start + " testStr " + end, helper.bold_str(" testStr "))

    def test_red(self):
        """Test red_str"""
        start = "\033[91m"
        end = "\033[0m"
        self.assertEqual(start + " testStr " + end, helper.red_str(" testStr "))

    def test_green(self):
        """Test green_str"""
        start = "\033[92m"
        end = "\033[0m"
        self.assertEqual(start + " testStr " + end, helper.green_str(" testStr "))

    def test_sprintf(self):
        string = "str"
        integer = 2
        floatN = 0.21

        self.assertEqual(
            f"Testing {string} {integer} {floatN}",
            helper.sprintf("Testing {:s} {:d} {:0.2f}", string, integer, floatN),
        )

    def test_printf(self):
        string = "str"
        integer = 2
        floatN = 0.21

        old_stdout = sys.stdout
        new_stdout = StringIO()
        sys.stdout = new_stdout

        helper.printf("Testing {:s} {:d} {:0.2f}", string, integer, floatN)

        sys.stdout = old_stdout
        printOutput = new_stdout.getvalue()

        self.assertEqual(f"Testing {string} {integer} {floatN}", printOutput)

    def test_printf_flush(self):
        string = "str"
        integer = 2
        floatN = 0.21

        old_stdout = sys.stdout
        new_stdout = StringIO()
        sys.stdout = new_stdout

        helper.printf("Testing {:s} {:d} {:0.2f}", string, integer, floatN, flush=True)

        sys.stdout = old_stdout
        printOutput = new_stdout.getvalue()

        self.assertEqual(f"Testing {string} {integer} {floatN}", printOutput)


class TestTerminal(unittest.TestCase):
    def test_clear_line(self):
        old_stdout = sys.stdout
        new_stdout = StringIO()
        sys.stdout = new_stdout

        helper.clear_line()

        sys.stdout = old_stdout
        printOutput = new_stdout.getvalue()

        self.assertEqual("\033[K", printOutput)

    def test_cursor_up_one_line(self):
        old_stdout = sys.stdout
        new_stdout = StringIO()
        sys.stdout = new_stdout

        helper.cursor_up_one_line()

        sys.stdout = old_stdout
        printOutput = new_stdout.getvalue()

        self.assertEqual("\033[F", printOutput)

    def test_cursor_up_lines_zero(self):
        old_stdout = sys.stdout
        new_stdout = StringIO()
        sys.stdout = new_stdout

        print("Hello, World!")
        helper.cursor_up_lines(0)

        sys.stdout = old_stdout
        printOutput = new_stdout.getvalue()

        self.assertEqual("Hello, World!\n", printOutput)

    # def test_cursor_up_lines_three(self):
    #     old_stdout = sys.stdout
    #     new_stdout = StringIO()
    #     sys.stdout = new_stdout

    #     print('Hello, World!')
    #     print('Hello, World!')
    #     print('Hello, World!')
    #     print('Hello, World!')
    #     helper.cursor_up_lines(3)

    #     sys.stdout = old_stdout
    #     printOutput = new_stdout.getvalue()
    #     print(printOutput)

    #     self.assertEqual('Hello, World!\n\n',
    #                      printOutput)

    @patch("sys.stdin", StringIO("y\n"))
    def test_prompt_yes_no_qustion(self):
        old_stdout = sys.stdout
        new_stdout = StringIO()
        sys.stdout = new_stdout

        helper.prompt_yes_no("Test question?")

        sys.stdout = old_stdout
        printOutput = new_stdout.getvalue()

        self.assertEqual(printOutput, "Test question? (y/n): ")

    @patch("sys.stdin", StringIO("y\n"))
    def test_prompt_yes_no_y(self):
        old_stdout = sys.stdout
        new_stdout = StringIO()
        sys.stdout = new_stdout

        promptReturn = helper.prompt_yes_no("Test question?")

        sys.stdout = old_stdout

        self.assertEqual(promptReturn, True)

    @patch("sys.stdin", StringIO("n\n"))
    def test_prompt_yes_no_n(self):
        old_stdout = sys.stdout
        new_stdout = StringIO()
        sys.stdout = new_stdout

        promptReturn = helper.prompt_yes_no("Test question?")

        sys.stdout = old_stdout

        self.assertEqual(promptReturn, False)

    @patch("sys.stdin", StringIO("yes\ny\n"))
    def test_prompt_yes_no_yes_y(self):
        old_stdout = sys.stdout
        new_stdout = StringIO()
        sys.stdout = new_stdout

        promptReturn = helper.prompt_yes_no("Test question?")

        sys.stdout = old_stdout

        self.assertEqual(promptReturn, True)

    @patch("sys.stdin", StringIO("yes\nn\n"))
    def test_prompt_yes_no_yes_n(self):
        old_stdout = sys.stdout
        new_stdout = StringIO()
        sys.stdout = new_stdout

        promptReturn = helper.prompt_yes_no("Test question?")

        sys.stdout = old_stdout

        self.assertEqual(promptReturn, False)


def prompt_yes_no(question):
    prompt = f"{question} (y/n): "
    ans = input(prompt).strip().lower()
    if ans not in ["y", "n"]:
        print(f"{ans} is invalid, please try again...")
        return prompt_yes_no(question)
    if ans == "y":
        return True
    return False


class TestIsWordPresent(unittest.TestCase):
    def test_empty_sentence(self):
        self.assertEqual(helper.is_word_present("", "test"), False)

    def test_empty_word(self):
        self.assertEqual(helper.is_word_present("My test sentence", ""), False)

    def test_true(self):
        self.assertEqual(helper.is_word_present("My test sentence", "test"), True)

    def test_false(self):
        self.assertEqual(helper.is_word_present("My test sentence", "Dragon"), False)

    def test_caseSensitive(self):
        self.assertEqual(helper.is_word_present("My test sentence", "Test"), False)


class TestSetupFiles(unittest.TestCase):
    def test_singleFolder_abs(self):
        folder = "tests/output/folder_abs/abs/"
        try:
            shutil.rmtree("tests/output/folder_abs")
        except FileNotFoundError:
            pass

        helper.setup_files(folder)

        self.assertEqual(os.path.isdir(folder), True)

    def test_singleFolder_rel(self):
        folder = "./tests/output/folder_rel/rel/"
        try:
            shutil.rmtree("tests/output/folder_rel")
        except FileNotFoundError:
            pass

        helper.setup_files(folder)

        self.assertEqual(os.path.isdir(folder), True)

    def test_singleFolder_space(self):
        folder = "./tests/output/folder_space/spa ce/"
        try:
            shutil.rmtree("tests/output/folder_space")
        except FileNotFoundError:
            pass

        helper.setup_files(folder)

        self.assertEqual(os.path.isdir(folder), True)

    def test_multipleFolder(self):
        folders = [
            "tests/output/folders/abs/",
            "./tests/output/folders/rel/",
            "./tests/output/folders/spa ce/",
        ]
        try:
            shutil.rmtree("tests/output/folders")
        except FileNotFoundError:
            pass

        helper.setup_files(folders)

        self.assertEqual(os.path.isdir(folders[0]), True)
        self.assertEqual(os.path.isdir(folders[1]), True)
        self.assertEqual(os.path.isdir(folders[2]), True)

    def test_singleFile_abs(self):
        singleFile = "tests/output/file/abs.txt"
        try:
            os.remove(singleFile)
        except FileNotFoundError:
            pass

        helper.setup_files(singleFile)

        self.assertEqual(os.path.isfile(singleFile), True)

    def test_singleFile_rel(self):
        singleFile = "./tests/output/file/rel.txt"
        try:
            os.remove(singleFile)
        except FileNotFoundError:
            pass

        helper.setup_files(singleFile)

        self.assertEqual(os.path.isfile(singleFile), True)

    def test_singleFile_space(self):
        singleFile = "./tests/output/file/spa ce.txt"
        try:
            os.remove(singleFile)
        except FileNotFoundError:
            pass

        helper.setup_files(singleFile)

        self.assertEqual(os.path.isfile(singleFile), True)

    def test_multipleFiles(self):
        files = [
            "tests/output/files/abs.txt",
            "./tests/output/files/rel.txt",
            "./tests/output/files/spa ce.txt",
        ]
        try:
            shutil.rmtree("tests/output/files")
        except FileNotFoundError:
            pass

        helper.setup_files(files)

        self.assertEqual(os.path.isfile(files[0]), True)
        self.assertEqual(os.path.isfile(files[1]), True)
        self.assertEqual(os.path.isfile(files[2]), True)


class TestNumbers(unittest.TestCase):
    def test_get_smallest_value_idx_all_positive(self):
        self.assertEqual(1, helper.get_smallest_value_idx([100, 10, 90]))

    def test_get_smallest_value_idx_all_negative(self):
        self.assertEqual(2, helper.get_smallest_value_idx([-60, -10, -90]))

    def test_get_smallest_value_idx_mixed(self):
        self.assertEqual(1, helper.get_smallest_value_idx([0, -10, 90]))


class TestJson(unittest.TestCase):
    def test_emptyStr(self):
        self.assertEqual(helper.is_json(""), False)

    def test_simple_ok(self):
        self.assertEqual(helper.is_json('{"is_json": "Yes"}'), True)

    def test_simple_bad(self):
        self.assertEqual(helper.is_json('"is_json": "Yes"}'), False)

    def test_nested_ok(self):
        self.assertEqual(helper.is_json('{"is_json": "Yes"}', "is_json"), True)

    def test_nested_bad(self):
        self.assertEqual(
            helper.is_json('{"is_json": "Yes", "badJson": x}', "is_json"), False
        )


if __name__ == "__main__":
    unittest.main()
