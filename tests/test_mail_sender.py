# import os
import sys
import unittest
from io import StringIO

import mail_sender

# from unittest.mock import patch


class TestMailSender(unittest.TestCase):
    def test_empty(self):
        old_stdout = sys.stdout
        new_stdout = StringIO()
        sys.stdout = new_stdout

        mail_sender.display_help()

        sys.stdout = old_stdout


if __name__ == "__main__":
    unittest.main()
