# Standard libraries
import json
import logging
import os
import sys
from enum import Enum

# 3rd party packages

# Local source


class Color(Enum):
    """Colors enum"""

    PURPLE = "\033[95m"
    CYAN = "\033[96m"
    DARKCYAN = "\033[36m"
    BLUE = "\033[94m"
    GREEN = "\033[92m"
    YELLOW = "\033[93m"
    RED = "\033[91m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"
    END = "\033[0m"


def bold_str(string: str) -> str:
    """Return bold string"""
    return Color.BOLD.value + string + Color.END.value


def red_str(string: str) -> str:
    """Return red string"""
    return Color.RED.value + string + Color.END.value


def green_str(string: str) -> str:
    """Return green string"""
    return Color.GREEN.value + string + Color.END.value


def sprintf(string, *args) -> str:
    """C like sprintf function"""
    return string.format(*args)


def printf(format, *args, flush=False) -> None:
    """C like printf function"""
    sys.stdout.write(sprintf(format, *args))
    if flush is True:
        sys.stdout.flush()


def clear_line() -> None:
    """Clear line on terminal"""
    sys.stdout.write("\033[K")  # Clear to the end of line


def cursor_up_one_line() -> None:
    """Move cursor one line up"""
    sys.stdout.write("\033[F")  # Cursor up one line


def cursor_up_lines(n: int) -> None:
    """Move cursorl up multiple lines"""
    while n > 0:
        clear_line()
        cursor_up_one_line()
        clear_line()
        n -= 1


def prompt_yes_no(question: str) -> bool:
    """Prompt yes/no"""
    prompt = f"{question} (y/n): "
    ans = input(prompt).strip().lower()
    if ans not in ["y", "n"]:
        print(f"{ans} is invalid, please try again...")
        return prompt_yes_no(question)
    if ans == "y":
        return True
    return False


def is_word_present(sentence: str, word: str) -> bool:
    """Function that returns true if the word is found"""
    if sentence == "" or word == "":
        return False

    # To break the sentence in words
    s = sentence.split(" ")

    for i in s:
        # Comparing the current word
        # with the word to be searched
        if i == word:
            return True

    return False


def setup_files(files: str | list[str]) -> None:
    """Check if file path exists, if it doesn't create it"""

    def setup_file(file_path):
        directory = os.path.dirname(file_path)
        if not os.path.exists(directory) and directory != "" and directory != ".":
            os.makedirs(directory)

        if not os.path.isdir(file_path):
            # Path is not a directory -> path is a file
            if not os.path.isfile(file_path):
                # File doesn't exist
                with open(file_path, "wt", encoding="UTF-8") as file:
                    # Just create empty file
                    file.write("")

    if isinstance(files, list) is True:
        # Argument is a list of file/folders
        for file_path in files:
            setup_file(file_path)
    else:
        # Argument is a string
        setup_file(files)


def get_smallest_value_idx(arr: list) -> int:
    """Get index of smallest number in array"""
    return arr.index(min(arr))


def logger_setup(
    log_file: str, level=logging.INFO, date_format="%Y/%m/%d %H:%M:%S", file_mode="a"
) -> None:
    """Logger setup"""
    setup_files(log_file)

    logging.basicConfig(
        filename=log_file,
        encoding="utf-8",
        level=level,
        format="%(asctime)s %(name)-12s %(levelname)-8s %(message)s",
        datefmt=date_format,
        filemode=file_mode,
    )


def is_json(json_str: str, obj_str=None) -> bool:
    """Check if string is in JSON format"""
    try:
        json_obj = json.loads(json_str)
        json.dumps(json_obj)

        if obj_str is not None:
            my_obj = json_obj[obj_str]
            my_obj  # To ignore "F841" warning

    except Exception:
        return False

    return True


def is_running_in_docker() -> bool:
    """Check if running in docker container"""
    path = "/proc/self/cgroup"
    return (
        os.path.exists("/.dockerenv")
        or os.path.isfile(path)
        and any("docker" in line for line in open(path))
    )
