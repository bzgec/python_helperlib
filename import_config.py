# Standard libraries
import json

# 3rd party packages
import yaml

# Local source


def get_json(file_name: str) -> dict | list:
    """Get custom configuration from JSON file"""

    data = {}

    with open(file_name, "rt", encoding="UTF-8") as f:
        data = json.load(f)

    return data


def get_yaml(file_name: str) -> dict | list:
    """Get custom configuration from YAML file"""

    data = {}

    with open(file_name, "rt", encoding="UTF-8") as f:
        data = yaml.safe_load(f)

    return data
