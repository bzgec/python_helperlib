=====================
Python helper library
=====================

:Info: Python helper library. It consists of functions which can be used in multiple projects.
:Authors:
    Bzgec

.. contents:: Table of Contents
   :depth: 2

.. role:: bash(code)
   :language: bash

Usage
=====

1. Create "App password" so you can use it instead of original mail password
   (needed since 2022/05/30 Google Policy Update) -
   `stackoverflow howto <https://stackoverflow.com/a/72529505/14246508>`__

2. Use this repository as *git* submodule: :bash:`git submodule add git@gitlab.com:bzgec/python_helperlib.git helperLib`

3. Import it to your *python* project as module:

   - `from helperLib.helper import sprintf, printf`
   - `import helperLib.mailSender as mailSender`
   - `from helperLib import importConfig`

4. You can also use `mailSender.py <mailSender.py>`__ from terminal

Development
===========

Before each commit check source code with :bash:`make check` command.

.. WARNING:: If there are some errors fix them before committing!

Or you can just run :bash:`make commit` which will run all the necessary commands before running
:bash:`git commit`.
Note that you still need to add files to index (:bash:`git add`).

Dependencies
------------
- `GNU make </www.gnu.org/software/make/>`__
- `pip` `requirements.dev.txt <quirements.dev.txt>`__
- `shellcheck` - a static analysis tool for shell scripts

License
=======

See the `LICENSE <LICENSE.adoc>`__ file for license rights and limitations (MIT).

TODO
====
