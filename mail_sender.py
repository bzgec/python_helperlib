#!/usr/bin/python

"""
https://geekflare.com/send-gmail-in-python/
https://stackoverflow.com/a/72529505/14246508 - create App password
"""
# Standard libraries
import getopt
import os
import smtplib
import ssl
import sys
import traceback
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from pathlib import Path

# 3rd party packages

# Local source


def display_help() -> None:
    """Print help message"""
    print(
        """\
Send emails through Gmail.

Tested with python 3.9.7.

How to use:
  python mailSender.py [OPTIONS]

"""
    )
    fileStr = os.path.join(os.path.dirname(__file__), "helpOptionsMailSender.txt")
    with open(fileStr, "r") as file:
        print(file.read())


# https://www.tutorialspoint.com/python/python_command_line_arguments.htm
# Ignore "C901 'parse_script_args' is too complex" - I don't think that it is too complex, also
# I don't know how mccabe's complexity could be reduced for this function
def parse_script_args(handle, argv):  # noqa: C901
    """Parse script arguments"""
    shortopts = "hf:p:r:x:c:a:"
    longopts = [
        "help",
        "sender=",
        "password=",
        "port=",
        "server=",
        "receiver=",
        "subject=",
        "content=",
        "attachment=",
    ]

    try:
        opts, args = getopt.getopt(argv, shortopts, longopts)
    except getopt.GetoptError:
        display_help()
        sys.exit(1)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            display_help()
            sys.exit(0)
        elif opt in ("-f", "--sender"):
            handle["sender_mail"] = arg
        elif opt in ("-p", "--password"):
            handle["pass"] = arg
        elif opt in ("--port"):
            handle["port"] = arg
        elif opt in ("--server"):
            handle["smtp_server"] = arg
        elif opt in ("-r", "--receiver"):
            handle["receiversMail"].append(arg)
        elif opt in ("-x", "--subject"):
            handle["subject"] = arg
        elif opt in ("-c", "--content"):
            handle["content"] = arg
        elif opt in ("-a", "--attachment"):
            handle["attachments"].append(arg)


class Mail:
    """Mail class"""

    def __init__(self, sender_mail: str, password: str, port=None, smtp_server=None):
        if port is None:
            port = self.get_default_port()

        if smtp_server is None:
            smtp_server = self.get_default_smtp_server()

        self.port = port
        self.smtp_server = smtp_server
        self.sender_mail = sender_mail
        self.password = password

    def get_default_port(self) -> int:
        """Get default port number"""
        return 465

    def get_default_smtp_server(self) -> str:
        """Get default SMTP server"""
        return "smtp.gmail.com"

    def send(
        self,
        receivers: str | list[str],
        subject: str,
        content: str,
        attachments: str | list[str] | None = None,
    ) -> bool:
        """Send mail"""
        mail_send_status = 1

        try:
            if isinstance(receivers, list) is False:
                # `receivers` in just a single string (not a list)
                receivers = [receivers]  # Create a list of strings

            # Attaching an attachment
            for receiver in receivers:
                ssl_context = ssl.create_default_context()
                service = smtplib.SMTP_SSL(
                    self.smtp_server, self.port, context=ssl_context
                )
                service.login(self.sender_mail, self.password)

                mail = MIMEMultipart("alternative")
                mail["Subject"] = subject
                mail["From"] = self.sender_mail
                mail["To"] = receiver

                text_mail = "Your mail reader does not support the report format."

                html_content = MIMEText(content.format(receiver.split("@")[0]), "html")
                text_content = MIMEText(
                    text_mail.format(receiver.split("@")[0]), "plain"
                )

                # Attaching messages to MIMEMultipart
                mail.attach(text_content)
                mail.attach(html_content)

                if attachments is not None:
                    if type(attachments) is not list:
                        # `attachments` in just a single file (not a list)
                        attachments = [attachments]  # Create a list of a single file

                    # Attaching an attachment
                    for attachment in attachments:
                        file_path = attachment
                        mime_base = MIMEBase("application", "octet-stream")
                        with open(file_path, "rb") as file:
                            mime_base.set_payload(file.read())
                        encoders.encode_base64(mime_base)
                        mime_base.add_header(
                            "Content-Disposition",
                            f"attachment; filename={Path(file_path).name}",
                        )
                        mail.attach(mime_base)

                service.sendmail(self.sender_mail, receiver, mail.as_string())
                service.quit()

            mail_send_status = 0

        except Exception:
            traceback_str = traceback.format_exc()
            print(traceback_str)

        return mail_send_status


def check_script_params(handle: dict) -> None:
    """Check if script arguments are OK"""
    if handle["sender_mail"] is None:
        print("Missing sender email!")
        sys.exit(2)

    if handle["pass"] is None:
        print("Missing sender email password!")
        sys.exit(2)

    if handle["receiversMail"] is None:
        print("Missing receiver email!")
        sys.exit(2)

    if handle["subject"] is None:
        print("Missing email subject!")
        sys.exit(2)


def main():
    """Main function"""
    handle = {
        "sender_mail": None,
        "pass": None,
        "port": None,
        "smtp_server": None,
        "receiversMail": [],
        "subject": None,
        "content": "",
        "attachments": [],
        "mailHandle": None,
    }

    if len(sys.argv) > 1:
        parse_script_args(handle, sys.argv[1:])
    else:
        display_help()
        sys.exit(1)

    # Check if script parameters are OK
    check_script_params(handle)

    # Initialise Mail module
    handle["mailHandle"] = Mail(
        handle["sender_mail"], handle["pass"], handle["port"], handle["smtp_server"]
    )

    # Send email
    mail_send_status = handle["mailHandle"].send(
        handle["receiversMail"],
        handle["subject"],
        handle["content"],
        handle["attachments"],
    )

    sys.exit(mail_send_status)


if __name__ == "__main__":
    main()
